# Final Project

## Kelompok 26

## Anggota Kelompok

-   Annisa Putri
-   Syafa Allisa Nisrina

## Tema Project

Web Review Buku

## ERD

![erd.jpeg](erd.jpeg?raw=true)

## Link Video

Link Video Demo https://youtu.be/MZDLzyJ27PA

## Alert
sweetalert dapat diimplementasikan, namun belum ada di video.
![alert.JPG](alert.JPG?raw=true)