<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genre';
    protected $fillable = ['jenis_genre'];

    public function buku()
    {
        return $this->hasMany('App\Buku');
    }
}
