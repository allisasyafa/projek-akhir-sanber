<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\profil;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    public function index(){

        $profil = profil::where('user_id',Auth::id())->first();
        
        return view('profil.edit', compact('profil'));

    }

    public function update($id, Request $request)
    {
        $request->validate([
    
            'umur' => 'required',
            'bio' => 'required',
         
        ]);

        $profil = profil::find($id);

        $profil->umur = $request->umur;
        $profil->bio = $request->bio;
        $profil->user_id = Auth::id();

        $profil->save();

       return redirect('/profil');
        

    }
}
