<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';
    protected $fillable = ['user_id','buku_id','isi'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function berita()
    {
        return $this->belongsTo('App\Berita');
    }

   
}
