@extends('layout.main')
@section('judul')
    Genre {{$genre->jenis_genre}}
@endsection

@section('content')

<div class="row">
    @forelse ($genre->buku as $item)
    <div class="col-3">
        <div class="card" >
            <img src="{{asset('images/'.$item->thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5>{{$item->judul}}</h5>
              <p class="card-text">{{Str::limit($item->deskripsi,50)}}</p>
              <form action="/buku/{{$item->id}}" method="POST">
                @method('DELETE')
                @csrf
                <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Read More</a>
           

            
            </div>
          </div>
    </div>
        
    @empty
        <h5>Tidak ada Buku</h5>
    @endforelse

</div>


    
 
@endsection