@extends('layout.main')
@section('judul')
    List Genre
@endsection

@section('content')

<a href="/genre/create" class="btn btn-primary mb-2" >Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Genre</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($genre as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->jenis_genre}}</td>
                        <td>
                            <form action="/genre/{{$value->id}}" method="POST">
                                @csrf
                                <a href="/genre/{{$value->id}}" class="btn btn-info">Show</a>
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
            
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    
@endsection