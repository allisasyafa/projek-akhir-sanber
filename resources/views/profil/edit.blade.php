@extends('layout.main')
@section('judul')
    Profil 
@endsection

@section('content')

<form action="profil/{{$profil->id}}" method="POST" enctype="multipart/form-data">
    @csrf 
    @method('put')
    <div class="form-group">
        <label >Nama User</label>
        <input type="text" class="form-control" value="{{$profil->user->name}}" disabled>
    </div>
    <div class="form-group">
        <label >Email</label>
        <input type="email" class="form-control" value="{{$profil->user->email}}" disabled>
    </div>
    <div class="form-group">
        <label >Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$profil->umur}}" placeholder="Masukkan Umur Anda">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" id="" class="form-control" cols="30" rows="10">{{$profil->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
   
    
    <button type="submit" class="btn btn-primary">Update</button>
</form>
    
@endsection