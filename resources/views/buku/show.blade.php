@extends('layout.main')
@section('judul')
    Detail Buku {{$buku->judul}}
@endsection

@section('content')

<div class="row">
   
    <div class="col-12">
        <div class="card" >
            <img class="ml-3 mt-3" src="{{asset('images/'.$buku->thumbnail)}}" width="200px" alt="...">
            <div class="card-body">
              <h5 class="card-title mb-2">{{$buku->judul}}</h5>
              <p class="card-text mt-4">{{$buku->deskripsi}}</p>
            </div>
          </div>
    </div>


</div>

<div class="row mt-2">
    <h4 class="ml-3">Komentar</h4>
    @forelse ($buku->komentar as $item)
        
    <div class="col-12">
    <div class="card">
      <div class="card-body">
        <h5>{{$item->user->name}}</h5>
        <p>{{$item->isi}}</p>
        <small>{{$item->created_at}}</small>

      </div>
    </div>
  </div>
  @empty
  <div class="col-12 ml-2">

    <h6>Tidak ada komentar</h6>
  </div>
    @endforelse
</div>

<div class="row">
  <div class="col-9">
    <form action="/komentar" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <input type="hidden" value="{{$buku->id}}" name="buku_id">
          <textarea name="isi" id="" class="form-control" placeholder="Masukkan Komentar"></textarea>
          @error('isi')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>

      <button type="submit" class="btn btn-primary">Tambah Komentar</button>
  </form>
      

  </div>

</div>


<a href="/buku" class="btn btn-secondary btn-sm mt-2 mb-4">Kembali</a>

    
@endsection