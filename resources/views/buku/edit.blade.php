@extends('layout.main')
@section('judul')
    Edit Buku
@endsection

@section('content')

<form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="form-group">
        <label >Judul Buku</label>
        <input type="text" class="form-control" name="judul" value="{{$buku->judul}}" placeholder="Masukkan Judul Buku">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="deskripsi" id="" class="form-control" cols="30" rows="10">{{$buku->deskripsi}}</textarea>
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Nama Penulis</label>
        <input type="text" class="form-control" name="penulis" value="{{$buku->penulis}}"  placeholder="Masukkan Nama Penulis">
        @error('penulis')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun Terbit</label>
        <input type="text" class="form-control" name="tahun" value="{{$buku->tahun}}" placeholder="Masukkan Tahun Terbit">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" id="" class="form-control">
            <option value="">--Pilih Genre--</option>

            @foreach ($genre as $item)
            @if ($item->id === $buku->genre_id)
                <option value="{{$item->id}}" selected>{{$item->jenis_genre}}</option>
                
            @else
            <option value="{{$item->id}}">{{$item->jenis_genre}}</option>
            @endif
            @endforeach
        </select>

        @error('genre_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Thumbnail Buku</label>
        <input type="file" class="form-control" name="thumbnail">
        @error('thumbnail')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

   
    
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
    
@endsection